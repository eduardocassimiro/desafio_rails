Rails.application.routes.draw do
  get 'home/index'
  root 'home#index'
  resources :reservations
  resources :books
  resources :genres
  resources :authors
  resources :clients
  resources :librarians
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
